package models;
	import java.util.ArrayList;
	import java.util.List;
	import play.data.validation.Constraints;
	import java.util.LinkedList;
	import play.mvc.PathBindable;
	import play.db.ebean.Model;
	import javax.persistence.*;
	import com.avaje.ebean.Page;
	import play.data.format.Formats;
	import java.util.Date;
	
@Entity
public class Product extends Model implements PathBindable<Product>{
	@Constraints.Required
	public String ean;
	
	@Id
    public Long id;
	
	@Constraints.Required
	public String name;
	
	public String description;
	
	@Formats.DateTime(pattern = "yyyy-MM-dd")
	public Date date;
	
	public byte[] picture;
	
	@OneToMany(mappedBy="product")
	public List<StockItem> stockItems;
    //public List<Tag> tags;
 
	public Product() {}
	public Product(String ean, String name, String description) {
		this.ean = ean;
		this.name = name;
		this.description = description;
	}
	@Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }
 
    @Override
    public String unbind(String key) {
        return ean;
    }
 
    @Override
    public String javascriptUnbind() {
        return ean;
    }
	
	
	public String toString() {
		return String.format("%s - %s", ean, name);
	}
	
	public static List<Product> findAll() {
		return find.all();
  }
 
	public static Product findByEan(String ean) {
		return find.where().eq("ean", ean).findUnique();
	}
 
  @ManyToMany
  public List<Tag> tags = new LinkedList<Tag>();
  
  public static Finder<Long,Product> find = new Finder<Long,Product>(
            Long.class, Product.class
    );
	
	public static Page<Product> find(int page) {  // trả về trang thay vì List
		return find.where()
              .orderBy("id asc")     // sắp xếp tăng dần theo id
              .findPagingList(10)    // quy định kích thước của trang
              .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
              .getPage(page);
	
	}
}