package models;

import java.util.List;
import java.util.ArrayList;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class StockItem extends Model {         
	@Id
    public Long id;
	
	@ManyToOne
    public Warehouse warehouse;	// Truong quan he noi voi Warehouse
 
    @ManyToOne
    public Product product;		// Truong quan he noi voi Product
 
    public Long quantity;
	
	public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
 
    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}