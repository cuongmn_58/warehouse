package models;

import java.util.List;
import java.util.ArrayList;
import play.db.ebean.Model;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.*;

@Entity
public class Warehouse extends Model {
	@Id
    public Long id;
	
    public String name;
	
	@OneToOne
	public Address address;
	
	@OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList(); //Truong quan he
	
	public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
 
    public String toString() {
        return name;
    }
}